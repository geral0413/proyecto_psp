function enviar_ajax(url, parametros) {
    return $.ajax({
        url: url,
        type: 'post',
        data: parametros
    })
}