<?php include(VIEWS . "default/head.php"); ?>

<form>
<div class="form-group">
    <label for="exampleFormControlSelect1">Tipo de mantenimiento</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>Seleccione...</option>
        <?php
            foreach ($tipo_mantenimiento as $key => $value){
                echo "<option value= ". $value['cod_mantenimiento'] ."> ". $value['nombre'] . "</option>";
            }
        ?>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Tipo de dispositivo movil</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Valor</label>
    <input class="form-control" type="text" placeholder="">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Observcaiones</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
  <button type="submit" class="btn btn-primary mb-2">Editar valor</button>
</form>

<?php include(VIEWS . "default/footer.php"); ?>