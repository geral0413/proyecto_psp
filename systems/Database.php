<?php


class Database {

    static private $_conexion;
    private $_host;
    private $_user;
    private $_pass;
    private $_dbName;
    private $_cursor;

    public function __construct() {
        $this->_host = DB_HOST;
        $this->_user = DB_USER;
        $this->_pass = DB_PASS;
        $this->_dbName = DB_NAME;
        $this->conexion();
    }

    private function conexion() {
        self::$_conexion = ADONewConnection('mysqli');
        self::$_conexion->Connect($this->_host, $this->_user, $this->_pass, $this->_dbName);
        self::$_conexion->setFetchMode(ADODB_FETCH_ASSOC);
//        self::$_conexion->debug = true;
        self::$_conexion->Execute("SET NAMES 'utf8'");
    }

    function querySimple($sql = "", $params = []){
        $this->_cursor = self::$_conexion->execute($sql, $params);
        return $this->_cursor->GetArray();
    }

}